<?php
/*
The background processes for firesale real estate plugin
*/
require_once "$dir/template.php";

function _wprfs_delivery_process() {

    global $wpdb;
    $timeStampOfLatestBlogPost = _wprfs_get_latest_listing_timestamp();
    $getSubscriptionsRequiringProcessingQuery = sprintf("SELECT COUNT(*) num FROM {$wpdb->prefix}wprfs_rep_listing_subscriptions WHERE last_published_post_date < %d",$timeStampOfLatestBlogPost);
    $results = $wpdb->get_results($getSubscriptionsRequiringProcessingQuery);
    $number = $results[0]->num;

    $numberOfBatches = ceil($number/100);

    for ($iter=0;$iter<$numberOfBatches;$iter++)
     {
        $batch_start = $iter*100;
        $getCurrentBatchQuery = sprintf("SELECT * FROM %swprfs_rep_listing_subscriptions WHERE last_published_post_date < %d LIMIT %d,100;",$wpdb->prefix, $timeStampOfLatestBlogPost, $batch_start);
        $batch = $wpdb->get_results($getCurrentBatchQuery);

        foreach ($batch as $subscription)
        {
            $post = _wprfs_get_post_to_deliver($subscription);


            if (!empty($post) && false != $post)
            {
                _wprfs_deliver_post($subscription,$post);
            }
            else
                continue;
        }
    }
    
    
}

function _wprfs_get_post_to_deliver($sub) {
    global $wpdb;

    $getListingQuery = sprintf("SELECT * FROM %sfsrep_listings WHERE listing_date_added> FROM_UNIXTIME(%s) ORDER BY listing_date_added ASC LIMIT 1",$wpdb->prefix, $sub->last_published_post_date);


    $listings = $wpdb->get_results($getListingQuery);

    if (count($listings) == 0)
        return false;
    else
        return $listings[0];    
}

function _wprfs_deliver_post($sub,$post)
        {
        global $wpdb;

    $subscriber = _wpr_subscriber_get($sub->sid);
    if (empty($subscriber))
        return;
    $nid = $subscriber->nid;
    
    //get the settings for this blog post for that newsletter
    $options = _wprfs_get_listing_settings($post->listing_id);
    //if there are delivery settings, honor them
    if (isset($options[$subscriber->nid])) {


        if ($options[$nid]['whetherSkipCompletely'] == 1) {
            _wprfs_update_subscription_to_listing($sub,$post);
            return;
        }
        if ($options[$nid]['whetherSkipFollowup'] == 1 && isReceivingFollowupPosts($subscriber->id))
            {
            _wprfs_update_subscription_to_listing($sub,$post);
            return;
        }
    }
    
    //if there are settings concerning the body of the blog post, process them
    $email = _wprfs_get_listing_email_body($post,$options[$nid],$sub);
    sendmail($subscriber->id,$email);

    _wprfs_update_subscription_to_listing($sub,$post,true);
}


function _wprfs_get_listing_email_body($listing,$options,$sub)
        {

         if ($options['whetherDisableCustomization'] == 1 || count($options) == 0 || ($options['whetherDisableCustomization'] == 0 && empty($options['subject'])) || (empty($options['htmlbody']) && empty($options['textbody']))) {

             
             $email = array("subject"=>"#{$listing->listing_id}: $listing->listing_label",
                 "htmlbody"=>_wprfs_listing_default_layout($listing),
                 "textbody"=>"",
                 "htmlenabled"=>1,
                 "attachimages"=>1,
                 "meta_key"=>"FSREP-{$sub->sid}-{$listing->listing_id}"
             );
             wpr_place_tags($sub->sid,$email);
             return $email;
         }
         else
             {
              $email = array("subject"=>$options['subject'],
                             "htmlbody"=>$options['htmlbody'],
                             "textbody"=>$options['textbody'],
                             "htmlenabled"=>1,
                             "attachimages"=>1,
                             "meta_key"=>"FSREP-{$sub->sid}-{$listing->listing_id}"
             );
              wpr_place_tags($sub->sid,$email);
              return $email;
         }
    
}



function _wprfs_update_subscription_to_listing($subscription,$listing,$all_fields=false) {
    global $wpdb;
    if (!$all_fields)
        $updateListingSubscriptionQuery = sprintf("UPDATE %swprfs_rep_listing_subscriptions SET last_published_post_date=UNIX_TIMESTAMP('%s') WHERE sid=%d",$wpdb->prefix,$listing->listing_date_added,$subscription->sid);
    else {
        $updateListingSubscriptionQuery = sprintf("UPDATE %swprfs_rep_listing_subscriptions SET last_processed_date=%d, last_published_postid=%d, last_published_post_date=UNIX_TIMESTAMP('%s') WHERE sid=%d",$wpdb->prefix,time(), $listing->listing_id, $listing->listing_date_added,$subscription->sid);
    }
    $wpdb->query($updateListingSubscriptionQuery);
}

function _wprfs_get_listing_settings($listing_id)
{
    global $wpdb;
    $getListingsQuery = sprintf("SELECT * FROM %swprfs_rep_listings_meta WHERE listing_id=%d",$wpdb->prefix,$listing_id);
    $results = $wpdb->get_results($getListingsQuery);
    $options = array();

    foreach ($results as $index=>$listing)
        {
        $options[$listing->nid] = unserialize($listing->other_info);
        $options[$listing->nid]['subject'] = $listing->subject;
        $options[$listing->nid]['textbody'] = $listing->textbody;
        $options[$listing->nid]['htmlbody'] = $listing->htmlbody;
    }
    return $options;
}


function _wprfs_get_latest_listing_timestamp() {
        global $wpdb;
    $getLatestListingTimeStamp = sprintf("SELECT listing_date_added FROM {$wpdb->prefix}fsrep_listings ORDER BY listing_date_added DESC LIMIT 1");
    $listing = $wpdb->get_results($getLatestListingTimeStamp);
    if (count($listing) > 0)
        {
        return strtotime($listing[0]->listing_date_added);
    }
    else
        {
        return 0;
    }
}
