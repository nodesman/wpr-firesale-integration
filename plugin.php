<?php

/*
Plugin Name: FireStorm Real Estate Listing Integration For WP Autoresponder
Author Name: Raj Sekharan
Author URI: http://www.nodesman.com
Plugin URI: http://www.nodesman.com
Description: Plugin to enable subscriptions to the real estate listings content type in the FireStorm Realestate plugin.
*/
$dir = dirname(__FILE__);
require_once "$dir/customize.php";
require_once "$dir/form.php";
require_once "$dir/subscriber.php";
require_once "$dir/import.php";
require_once "$dir/background.php";
require_once "$dir/template.php";
//ensure that the plugin's functions become available only if the WP Autoresponder plugin is enabled.



register_activation_hook(__FILE__,"_fswpr_install");

   
    add_action("init","_fswpr_init");
    function _fswpr_init() {
        //get the plugin to enqueue the script if the page is the add new listing page.

        $active_plugins = get_option("active_plugins");
        $whetherWPRActive = false;
        
        foreach ($active_plugins as $plugin) {

            if (preg_match("@wpresponder.php$@",$plugin)) {
                $whetherWPRActive = true;
            }
            
        }
        if ($whetherWPRActive == false) {
                add_action("admin_notices", "_wprfs_no_wpr");
                return;
        }


       add_action("fsre_meta_box","fs_meta_real");
       add_action("fs_rep_listing_created","_wprfs_listing_created",1,1);
       add_action("fsrep_load_listing_edit","_wprfs_listing_edit_loading",1,1);
       add_action("fsrep_listing_save","_wprfs_listing_saved",1,1);
       add_action("_wprfs_deliver_listings","_wprfs_delivery_process");

        $url = get_bloginfo("url");
        wp_register_script( "wprfs-tabber", "$url/?wprfs-file=tabber.js");
        add_action('admin_init','wprfs_enqueue_admin_scripts');
        if (isset($_GET['wprfs-file'])) {
            switch ($_GET['wprfs-file'])
            {
                case 'tabber.js':
                    $dir = dirname(__FILE__);
                    readfile("$dir/tabber.js");
                    exit;
                    break;
            }
        }
    }


    function wprfs_enqueue_admin_scripts() {
        $page = $_GET['page'];
        if ($page == "fsrep_listings") {
            wp_enqueue_script("wprfs-tabber");
            $containingdirectory = basename(dirname(__FILE__));
            wp_enqueue_script("wpresponder-ckeditor");
            wp_enqueue_script("wpresponder-addedit");

            wp_enqueue_style("wpresponder-tabber","/".PLUGINDIR."/".$containingdirectory."/tabber.css");
        }
    }

    add_action("fsrep_listing_add_post", "fsrep_real_estate_listing_post");

    function fsrep_real_estate_listing_post() {
        //store the listings in a private variable
        $listings = _wprfs_get_listing_save_options();
        $GLOBALS['_fs_email_settings'] = $listings;
        
    }
    
    function fs_meta_real() {


        ?>
    <p><table class="widefat page fixed" cellspacing="0" border="1">
    <tr>
       <th>E-mail Newsletter Settings</th>
    </tr>
    <tr>
      <td><?php _wprfs_getNewsletterOptions(); ?></td>
    </tr>
    </table>
    </p>
    <?php

    }

    function _wprfs_listing_saved($listing_id) {
        global $wpdb;
        $deleteExistingListingsQuery = sprintf("DELETE FROM %swprfs_rep_listings_meta WHERE listing_id=%d",$wpdb->prefix,$listing_id);
        $wpdb->query($deleteExistingListingsQuery);
        _wprfs_listing_created($listing_id);
    }

    function _wprfs_listing_edit_loading($listing_id)
    {
        global $wpdb;

        $newsletters = $wpdb->get_results(sprintf("SELECT id FROM %swpr_newsletters",$wpdb->prefix));

        foreach ($newsletters as $newsletter)
        {
            $getListingSettings = sprintf("SELECT * FROM {$wpdb->prefix}wprfs_rep_listings_meta WHERE listing_id=%d AND nid=%d",$listing_id,$newsletter->id);
            $listing = $wpdb->get_results($getListingSettings);
            $settings = array();
            if (count($listing) > 0)
                {
                //assemble the listing in the format used by the interface
                $settings = unserialize($listing[0]->other_info);
                $settings['subject'] = $listing[0]->subject;
                $settings['textbody'] = $listing[0]->textbody;
                $settings['htmlbody'] = $listing[0]->htmlbody;
            }
            else
                {
                $settings[$newsletter->id] = array(
                    "whetherSkipCompletely"=>0,
                    "whetherSkipFollowup"=>1,
                    "whetherDisableCustomization"=>1,
                    "enable_html"=>0,
                    "attachimages"=>0,
                    "subject"=>"",
                    "textbody"=>"",
                    "htmlbody"=>""
                );
            }
            $all_settings[$newsletter->id] = $settings;

        }
        $GLOBALS['_fs_email_settings'] = $all_settings;


    }


    function _wprfs_listing_created($listingID) {
      global $wpdb;
      
      $options = _wprfs_get_listing_save_options();

      foreach ($options as $nid=>$option)
	{
	  $otherInfo = $option;
	  unset($otherInfo['subject']);
	  unset($otherInfo['textbody']);
	  unset($otherInfo['htmlbody']);
	  $other_info = (serialize($otherInfo));
	  $addOptionsQuery = sprintf("INSERT INTO  {$wpdb->prefix}wprfs_rep_listings_meta (listing_id, nid, subject, htmlbody, textbody, other_info) VALUES (%d, %d, '%s', '%s', '%s', '%s');",$listingID,$nid, $option['subject'],$option['htmlbody'],$option['textbody'], $other_info);
	  $wpdb->query($addOptionsQuery);
	}
	

      
    }



    function _wprfs_get_listing_save_options() {
      global $wpdb;


            $getNewslettersQuery = sprintf("SELECT id FROM {$wpdb->prefix}wpr_newsletters");
	    $newsletterList = $wpdb->get_results($getNewslettersQuery);

	    $listingEmailOptions = array();

	    foreach ($newsletterList as $nid)
	      {
		$nid = $nid->id;
		$options = array("whetherSkipCompletely" => (isset($_POST['skipnewsletter-'.$nid]))?1:0,
			 "whetherSkipFollowup" => (isset($_POST['skipfollowup-'.$nid]))?1:0,
			 "whetherDisableCustomization" => (isset($_POST['disablecustomization-'.$nid]))?1:0,
			 "subject" => $_POST['subject-'.$nid],
			 "textbody" => $_POST['textbody-'.$nid],
			 "enable_html" => (isset($_POST['htmlenable-'.$nid]))?1:0,
			 "attachimages"=> (isset($_POST['attachimages-'.$nid]))?1:0,
			 "htmlbody" => $_POST['htmlbody-'.$nid]
			 );


		$listingEmailOptions[$nid]=$options;
		
	      }
	    return $listingEmailOptions;
    }

    



    function _fswpr_install() {
        global $wpdb;
        ////create the table
        $createTablesQueries[] = sprintf("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}wprfs_rep_listing_subscriptions` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `sid` int(11) NOT NULL,
          `type` enum('all','cat') NOT NULL,
          `catid` int(11) NOT NULL,
          `last_processed_date` int(11) NOT NULL,
          `last_published_postid` int(11) NOT NULL,
          `last_published_post_date` bigint(20) NOT NULL DEFAULT '0',
          `pending_reprocess` tinyint(4) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`),
          UNIQUE KEY `unique_re_subscriptions_per_subscriber` (`sid`,`type`,`catid`)
        )");

        $createTablesQueries[] = sprintf("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}wprfs_rep_listings_meta` (
          `listing_id` int(11) NOT NULL,
          `nid` INT NOT NULL,
          `subject` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          `htmlbody` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          `textbody` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          `other_info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          UNIQUE KEY `unique_newsletter_options_for_listings` (`listing_id`,`nid`)
          );");

        $createTablesQueries[] = sprintf("CREATE TABLE IF NOT EXISTS `%swprfs_subscription_forms` (
              `form_id` int(11) NOT NULL,
              PRIMARY KEY (`form_id`)
            )",$wpdb->prefix);

        foreach ($createTablesQueries as $query ) {
            $wpdb->query($query);
        }
        
        if (false == wp_get_schedule("_wprfs_deliver_listings",$cron['arguments']))
	{
           wp_schedule_event(time(), "hourly","_wprfs_deliver_listings");
        }
    }



function _wprfs_no_wpr()
{
    ?>
    <div class="error fade">
        <p style="padding:5px;">
            <strong>Important: </strong> The WP Autoresponder plugin is not active. The FireStorm real estate listings integration plugin requires WP Autoresponder to function. 
        </p>
    </div>
    <?php
}